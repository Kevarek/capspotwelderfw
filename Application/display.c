/*
 * display.c
 *
 *  Created on: Nov 27, 2019
 *      Author: Standa
 */


#include "display.h"
#include "ssd1306.h"
#include "bbb.h"
#include <string.h>
#include <stdio.h>
#include "measurement.h"
#include "SpotWelder.h"


//INIT SCREEN INFO --- 18 char per line max
#define DEVSTRING	"Spot Welder 01"
#define NAMESTRING	"By Stanislav Subrt"
#define REVSTR		"HWAA, FWAA, 200820"


#define TMPSIZE	32	//18 is the length of line
char Tmp[TMPSIZE];
char Line[TMPSIZE];


/*
 * Draws frame around specified area (optionally filled with black color)
 */
void disp_DrawFrame(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t isFilled){

	for(uint8_t i=y1; i<=y2; i++){
		if(isFilled) for(uint8_t j=x1; j<=x2; j++) ssd1306_DrawPixel(j, i, Black);
		ssd1306_DrawPixel(x1, i, White);
		ssd1306_DrawPixel(x2, i, White);
	}

	for(uint8_t i=x1; i<=x2; i++){
		ssd1306_DrawPixel(i, y1, White);
		ssd1306_DrawPixel(i, y2, White);
	}
}


/*
 * Draws "saved" pop-up window in the center of the screen
 */
/*
void disp_DrawSavedPopup(void){

	  //SECOND LINE
	strcpy(Line + 0, "      Saved!      ");
	disp_DrawFrame(26, 8, 100, 22, 1);
	ssd1306_SetCursor(28, 10);
	ssd1306_WriteString(Line, Font_7x10, White);
	ssd1306_UpdateScreen();
}
*/

/*
 * Draws loading screen with battery voltage and core temperature
 */
void disp_DrawInitScreen(void){
	ssd1306_Fill(Black);
	ssd1306_SetCursor(0, 0);
	ssd1306_WriteString(DEVSTRING, Font_7x10, White);

	ssd1306_SetCursor(0, 10);
	ssd1306_WriteString(NAMESTRING, Font_7x10, White);

	ssd1306_SetCursor(0, 20);
	ssd1306_WriteString(REVSTR, Font_7x10, White);
	ssd1306_UpdateScreen();
}



extern uint16_t GetReqEnergy(void);
extern uint16_t GetCapEnergy(void);
extern uint16_t GetMaxCapEnergy(void);

/*
 * Status is max 12 char long
 */
void disp_DrawDefScreen(char *status){
	uint16_t VBat, ECapSet, ECapCur, ECapMax;

	VBat = meas_GetVbat() / 1000;
	ECapSet = GetReqEnergy();
	ECapCur = GetCapEnergy();
	ECapMax = GetMaxCapEnergy();

	ssd1306_Fill(Black);

	//FIRST LINE -
	sprintf(Line, "Vb=%02dV - %s", VBat, status);
	ssd1306_SetCursor(0, 0);
	ssd1306_WriteString(Line, Font_7x10, White);

	//SECOND LINE
	sprintf(Line, "Set  Get Lim");
	ssd1306_SetCursor(0, 11);
	ssd1306_WriteString(Line, Font_7x10, White);

	//THIRD LINE
	snprintf(Line, sizeof(Line), "%02d   %02d   %02d  [J]", ECapSet, ECapCur, ECapMax);
	ssd1306_SetCursor(0, 21);
	ssd1306_WriteString(Line, Font_7x10, White);

	ssd1306_UpdateScreen();
}


/*
 *
 */
void disp_DrawResultsScreen(uint8_t weldResult){
	int16_t T75, T50, T25, EDis;

	sw_GetReport(&T75, &T50, &T25, &EDis);

	ssd1306_Fill(Black);

	//FIRST LINE
	if(weldResult == 1){	//Charging failed
		sprintf(Line, "Charg. fail! E=%02dJ", 0);
	}
	else if(weldResult == 2){	//Discharging failed
		sprintf(Line, "Weld. fail! E=%02dJ", EDis);
	}
	else{	//0 is success
		sprintf(Line, "Success! E=%02dJ", EDis);
	}

	ssd1306_SetCursor(0, 0);
	ssd1306_WriteString(Line, Font_7x10, White);

	//SECOND LINE
	sprintf(Line, "T75 T50 T25");
	ssd1306_SetCursor(0, 11);
	ssd1306_WriteString(Line, Font_7x10, White);

	//THIRD LINE
	snprintf(Line, sizeof(Line), "%03d %03d %03d [.1ms]", T75, T50, T25);
	ssd1306_SetCursor(0, 21);
	ssd1306_WriteString(Line, Font_7x10, White);

	ssd1306_UpdateScreen();
}
