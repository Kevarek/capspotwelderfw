/*
 * SpotWelder.h
 *
 *  Created on: Apr 20, 2020
 *      Author: Standa
 */

#ifndef SPOTWELDER_H_
#define SPOTWELDER_H_


#include "stdint.h"


#define SW_BANK_ENERGY_LIMIT_MJ	63


extern void sw_Init(void);
extern int8_t sw_Charge(uint16_t capVoltage_mV);
extern int8_t sw_Weld(uint8_t duration_ms);
extern void sw_GetReport(int16_t *repT75, int16_t *repT50, int16_t *repT25, int16_t *eDis);
extern void sw_ResetReport(void);


extern uint16_t sw_CalcCapVoltageFromEnergy(uint8_t energy_mJ);
extern uint8_t sw_GetEnergy(uint16_t vbat_mV);

#endif /* SPOTWELDER_H_ */
