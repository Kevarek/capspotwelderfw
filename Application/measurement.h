/*
 * measurement.h
 *
 *  Created on: Oct 28, 2019
 *      Author: Standa
 */

#ifndef MEASUREMENT_H_
#define MEASUREMENT_H_

#include "error.h"


extern void meas_Init(void);
extern uint16_t meas_GetVbat(void);
extern uint16_t meas_GetVcap(void);
extern uint16_t meas_GetVe2(void);
extern void meas_ResetConvCntr(void);
extern uint32_t meas_GetConvCntr(void);

extern void meas_EndOfADCReadingHandler(void);


#endif /* MEASUREMENT_H_ */
