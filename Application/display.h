/*
 * display.h
 *
 *  Created on: Nov 27, 2019
 *      Author: Standa
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_


#include <stdint.h>


extern void disp_DrawSavedPopup(void);
extern void disp_DrawInitScreen(void);
extern void disp_DrawDefScreen(char *status);
extern void disp_DrawResultsScreen(uint8_t weldResult);


#endif /* DISPLAY_H_ */
